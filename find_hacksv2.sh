#!/bin/bash


#d="ome/wwwroot/$1";
d=$1

echo "beginning scan... please be patient...";
printf "Looking for problems in '$d' ... \n\n";





secs=$(date +%s)

# Scan for known backdoors
grep -rnl  $d -e "Security Angel Team" 					> /tmp/f1_$secs &
grep -rnl  $d -e ".*eval(gzinflate(base64_decode('HJ3HkqNQEkU.*" 	> /tmp/f2_$secs &
grep -rnl $d -e ".*preg_replace\s*(\"/\.\*/e\".*" 			> /tmp/f3_$secs &
grep -rnl $d -e ".*preg_replace(\"/Bs7n.*" 				> /tmp/f4_$secs &
grep -rnl $d -e ".*preg_replace(\"/IUDo.*" 				> /tmp/f5_$secs &

# scan for leaks
grep -rnl  $d -e ".*phpinfo.*" 									> /tmp/f7_$secs &
find $d | grep -e ".*\.php..*" 									> /tmp/f8_$secs &

# scan for dangerous suid/sgid to gain root access
find / -perm  /u=s,g=s -user root 	> /tmp/f9_$secs &

# Scan for potential PHP injection
varInParams="[^;]*(\$|\^|\\x|base64|inflate|/e).*;.*";
grep -Ernl $d -e ".*eval$varInParams" 		> /tmp/f6a_$secs &
grep -Ernl $d -e ".*system$varInParams" 	> /tmp/f6b_$secs &
grep -Ernl $d -e ".*passthru$varInParams" 	> /tmp/f6c_$secs &
grep -Ernl $d -e ".*preg_replace$varInParams" 	> /tmp/f6d_$secs &
grep -Ernl $d -e ".*exec$varInParams" 		> /tmp/f6e_$secs &
grep -Ernl $d -e ".*assert$varInParams" 	> /tmp/f6f_$secs &
grep -Ernl $d -e ".*require[_once]?$varInParams" > /tmp/f6g_$secs &
grep -Ernl $d -e ".*include[_once]?$varInParams" > /tmp/f6h_$secs &
grep -Ernl $d -e ".*create_function$varInParams" > /tmp/f6i_$secs &
grep -Ernl $d -e ".*popen$varInParams" 		> /tmp/f6j_$secs &
grep -Ernl $d -e ".*proc_open$varInParams" 	> /tmp/f6k_$secs &

# Scan for potential SQL injection
grep -Ernl $d -e ".*\$sql.*=[^;]*\$_.*;.*" 	> /tmp/f11_$secs &



# Scan for permission denied
find $d -name "*.php" -perm 000 > /tmp/f10_$secs

# Wait for scans to finish and collect
time wait > /tmp/t_$secs;

f1=$(cat /tmp/f1_$secs)
f2=$(cat /tmp/f2_$secs)
f3=$(cat /tmp/f3_$secs)
f4=$(cat /tmp/f4_$secs)
f5=$(cat /tmp/f5_$secs)
f7=$(cat /tmp/f7_$secs)
f8=$(cat /tmp/f8_$secs)
f9=$(cat /tmp/f9_$secs)
f10=$(cat /tmp/f10_$secs)
f11=$(cat /tmp/f11_$secs)
f6a=$(cat /tmp/f6a_$secs)
f6b=$(cat /tmp/f6b_$secs)
f6c=$(cat /tmp/f6c_$secs)
f6d=$(cat /tmp/f6d_$secs)
f6e=$(cat /tmp/f6e_$secs)
f6f=$(cat /tmp/f6f_$secs)
f6g=$(cat /tmp/f6g_$secs)
f6h=$(cat /tmp/f6h_$secs)
f6i=$(cat /tmp/f6i_$secs)
f6j=$(cat /tmp/f6j_$secs)
f6k=$(cat /tmp/f6k_$secs)

rm -f /tmp/f1_$secs
rm -f /tmp/f2_$secs
rm -f /tmp/f3_$secs
rm -f /tmp/f4_$secs
rm -f /tmp/f5_$secs
rm -f /tmp/f7_$secs
rm -f /tmp/f8_$secs
rm -f /tmp/f9_$secs
rm -f /tmp/f10_$secs
rm -f /tmp/f11_$secs
rm -f /tmp/f6a_$secs
rm -f /tmp/f6b_$secs
rm -f /tmp/f6c_$secs
rm -f /tmp/f6d_$secs
rm -f /tmp/f6e_$secs
rm -f /tmp/f6f_$secs
rm -f /tmp/f6g_$secs
rm -f /tmp/f6h_$secs
rm -f /tmp/f6i_$secs
rm -f /tmp/f6j_$secs
rm -f /tmp/f6k_$secs

printf "Backdoors PhpSpy :\n";
echo "$f1"

printf "\n\nBackdoors C99MadShell :\n"
echo "$f2"


printf "\n\nBackdoors WSO 2.4 :\n"
echo "$f3"


printf "\n\nBackdoor autre 1 :\n"
echo "$f4"
# | sed 's/$d\//\n$d\//g'


printf "\n\nBackdoor autre 2 :\n"
echo "$f5"


printf "\n\nAutres potentielles backdoors/menaces d'execution de code malveillant :\n"
echo "$f6a"
echo "$f6b"
echo "$f6c"
echo "$f6d"
echo "$f6e"
echo "$f6f"
echo "$f6g"
echo "$f6h"
echo "$f6i"
echo "$f6j"
echo "$f6k"



printf "\n\nFichiers php a permissions 000 :\n"
echo "$f10"


printf "\n\nAttention possible injection SQL :\n"
echo "$f11"


printf "\n\nAttention potentiel leak de phpinfo :\n"
echo "$f7"
# | sed 's/\s\//\//g'


printf "\n\nAttention leak de code php :\n"
echo "$f8"

printf "\n\nAttention possible escalation de privileges (SetUID/SetGID root) :\n"
echo "$f9"



printf "\n\nTime taken for scan : \n"
cat /tmp/t_$secs
rm -f /tmp/t_$secs
