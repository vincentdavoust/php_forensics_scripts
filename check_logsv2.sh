#!/bin/bash

flename="logs/access_all"
tmp=$(date +%s)
if [ $filename -e "*.gz" ]
then
	gunzip $filename /tmp/${filename}_$tmp
	filename="/tmp/${filename}_$tmp"
fi

failed=$(grep $filename -E -e ".*\.(zip|rar|gz|sql|db|tar|log|7z|bak|dmp) .{0,15}\" [^2][0-9]{2}" | grep -Eo -e "[0-9\.]{7,15}" | sort | uniq -c | sort -rn | awk '$1>10')
failedIp=$(echo "$failed" | grep -Eo -e "[0-9\.]{7,15}")
successIp=$(grep $filename -E -e ".*\.(zip|rar|gz|sql|db|tar|log|7z|bak|dmp) .{0,15}\" 200" | grep -Eo -e "[0-9\.]{7,15}" | sort | uniq -c | sort -rn)

alert=$(echo "$failedIp$successIp | surt | uniq -c | awk '$1>1')

echo $alert
