# php_forensics_scripts

A collection of bash scripts to scan a server for php backdoors and data leaks.
Tailored for Apache servers running PHP

!!! These scripts may find some backdoors and data leaks, but it is in NO WAY EXHAUSTIVE !!!
!!! NO GUARANTY IS PROVIDED !!!


Please use these scripts with caution, and be prudent with their outputs
